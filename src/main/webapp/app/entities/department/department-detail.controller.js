(function() {
    'use strict';

    angular
        .module('edgApp')
        .controller('DepartmentDetailController', DepartmentDetailController);

    DepartmentDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Department'];

    function DepartmentDetailController($scope, $rootScope, $stateParams, entity, Department) {
        var vm = this;

        vm.department = entity;

        var unsubscribe = $rootScope.$on('edgApp:departmentUpdate', function(event, result) {
            vm.department = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
