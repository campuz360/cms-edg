(function() {
    'use strict';

    angular
        .module('edgApp')
        .controller('DepartmentDialogController', DepartmentDialogController);

    DepartmentDialogController.$inject = ['$timeout', '$scope', '$stateParams','$state', 'AlertService', 'entity', 'Department'];

    function DepartmentDialogController ($timeout, $scope, $stateParams,$state, AlertService, entity, Department) {
        var vm = this;

        vm.department = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        
        function save () {
            vm.isSaving = true;
            if (vm.department.id !== null) {
                Department.update(vm.department, onSaveSuccess, onSaveError);
            } else {
                Department.save(vm.department, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('edgApp:departmentUpdate', result);
            vm.isSaving = false;
            goBack();
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        function goBack() {
            window.history.back();
        }


    }
})();

