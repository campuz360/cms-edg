(function() {
    'use strict';

    angular
        .module('edgApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
