'use strict';

describe('Controller Tests', function() {

    describe('Tasks Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTasks, MockUsers;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTasks = jasmine.createSpy('MockTasks');
            MockUsers = jasmine.createSpy('MockUsers');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Tasks': MockTasks,
                'Users': MockUsers
            };
            createController = function() {
                $injector.get('$controller')("TasksDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'edgApp:tasksUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
